import { Todo, TodoAttributes, TodoModel, TodoStatus } from '../model'

export type GetManyTodosCondition = Partial<Pick<TodoAttributes, 'task' | 'status'>>
export type GetManyTodosOptions = {
  skip?: number
  limit?: number
  orderBy?: string
  orderDesc?: boolean
}

export const processTodoCreating = async (data: Pick<TodoAttributes, 'task'>): Promise<void> => {

  await TodoModel.create({ ...data, status: TodoStatus.new })
}

export const processTodoUpdating = async (id: string, data: Pick<TodoAttributes, 'task' | 'status'>): Promise<void> => {

  await TodoModel.updateOne({ _id: id }, data)
}

export const processTodoDeleting = async (id: string) => {

  await TodoModel.deleteOne({ _id: id })
}

export const findOneTodo = async (id: string): Promise<Todo> => {

  return TodoModel.findById(id)
}

export const findManyTodos = async (condition: GetManyTodosCondition = {}, options: GetManyTodosOptions = {}) => {
  const query = TodoModel.find(condition)

  if (options.skip) { query.skip(options.skip) }
  if (options.limit) { query.limit(options.limit) }
  if (options.orderBy) { query.sort([options.orderBy, options.orderDesc ? -1 : 1]) }

  return query.exec()
}
