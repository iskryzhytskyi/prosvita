import * as path from 'path'
import * as express from 'express'
import * as compress from 'compression'
import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import * as swaggerUiExpress from 'swagger-ui-express'
import * as  yaml from 'yamljs'

import config from './config'
import { createLogger } from './shared'
import { connect } from './model'

import { initRoutes } from './routes'

// eslint-disable-next-line no-undef
const swaggerDocument = yaml.load(path.resolve(__dirname, 'swagger.yaml'))

const logger = createLogger({ name: 'index' })

const app: express.Express = express()

const connectToDb = () => connect({
  host: config('MONGO_HOST'),
  port: config('MONGO_PORT'),
  database: config('MONGO_DATABASE'),
  user: config('MONGO_USER'),
  password: config('MONGO_PASSWORD'),
})

app.use(compress())
app.use(cors())
app.use(bodyParser.json({ limit: '50kb' }))
app.use(bodyParser.urlencoded({ extended: true, limit: '50kb' }))

const runApp = async () => {
  try {
    await connectToDb()
    const router = await initRoutes()
    app.use('/api/v1/documentation', swaggerUiExpress.serve, swaggerUiExpress.setup(swaggerDocument))
    app.use('/api/v1', router)
    app.listen(config('APP_PORT'))
    logger.info({ port: config('APP_PORT') }, 'Server running')
  } catch (e) {
    logger.warn(e)
  }
}

runApp()
