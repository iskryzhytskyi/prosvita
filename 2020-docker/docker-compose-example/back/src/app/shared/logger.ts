export type LoggerOptions = {
  name: string
}

export type LoggerMethod = (params: { [key: string]: any }, message?: string) => void

export type Logger = {
  info: LoggerMethod
  warn: LoggerMethod
  error: LoggerMethod
}

export const createLogger = (options: LoggerOptions): Logger => {
  return ['info', 'warn', 'error'].reduce(
    (res: any, type: string) => ({ ...res, [type]: (params, message) => { console.log({ ...options, type }, params, message) } }),
    {},
  )
}

