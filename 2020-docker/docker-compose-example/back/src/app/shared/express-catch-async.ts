import { Handler } from 'express'
import { Logger } from './logger'

export const catchAsync = (logger: Logger, errorMessage: string, fn: Handler): Handler => async (req, res, next) => {
  try {
    await fn(req, res, next)
  } catch (err) {
    logger.error({ err }, errorMessage)

    next(err)
  }
}
