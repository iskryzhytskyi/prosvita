import { Request, Response } from 'express'
import { TodoAttributes, TodoModel, TodoStatus } from '../model'
import { catchAsync, createLogger, HttpStatuses } from '../shared'
import {
  processTodoCreating,
  processTodoUpdating,
  processTodoDeleting,
  findOneTodo,
  findManyTodos,
} from '../services/todos'


const logger = createLogger({ name: 'handlers/categories' })


export const create = catchAsync(logger, 'todos|create handling failed',async (req: Request, res: Response) => {
  await processTodoCreating(req.body)

  res.sendStatus(HttpStatuses.CREATED)
})

export const update = catchAsync(logger, 'todos|update handling failed',async (req: Request, res: Response) => {
  await processTodoUpdating(req.params.id, req.body)

  res.sendStatus(HttpStatuses.NO_CONTENT)
})

export const remove = catchAsync(logger, 'todos|remove handling failed',async (req: Request, res: Response) => {
  await processTodoDeleting(req.params.id)

    res.send(HttpStatuses.NO_CONTENT)
})

export const get = catchAsync(logger, 'todos|get handling failed',async (req: Request, res: Response) => {
  const { id } = req.params

  if (id) {
    const todo = await findOneTodo(id)

    todo ? res.send(todo) : res.sendStatus(HttpStatuses.NOT_FOUND)

    return
  }

  const options = {
    limit: req.query.limit ? Number(req.query.limit) : 10,
    skip: req.query.skip ? Number(req.query.skip) : 0,
    orderBy: req.query.orderBy,
    orderDesc: req.query.orderDesc
  }

  const condition: any = { }

  if (req.query.task) { condition.task = new RegExp(req.query.task) }


  const todos = await findManyTodos(condition, options)
  res.send(todos)
})
